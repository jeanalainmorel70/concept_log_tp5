from typing import Optional
import json
import requests



from fastapi import FastAPI

app = FastAPI()


@app.get("/")
def read_root():
    requete = requests.get("https://world.openfoodfacts.org/api/v0/product/3256540001305.json")
    return isVegan(requete.json())

@app.get("/{product_id}")
def is_product_vegan():
    requete = requests.get("https://world.openfoodfacts.org/api/v0/product/{product_id}.json")
    return isVegan(requete.json())



def isVegan(requestAsJson):
    try:
        ingredients = requestAsJson["product"]["ingredients"]
    except KeyError :
        return {"response" :"N/A"}

    for ingredient in ingredients:
        try:
            ingredient["vegan"]
        except KeyError :
            return {"response" :"N/A"}
        if not ingredient["vegan"]=='yes':
            return {"response" :"False"}
        
            
    return {"response" :"True"}
        


